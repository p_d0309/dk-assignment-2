<?php

namespace App\Http\Controllers;

use App\Models\Attendance;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Response;

class AttendanceController extends Controller
{
    public function index(Request $request) {

    }

    public function storeAttendance(Request $request) {
        $todaysAttendance = Attendance::whereDate('created_at',Carbon::now())->where('user_id', $request->user_id)->first();
        if(!$todaysAttendance) {
            $attendance = new Attendance;
            $attendance->user_id = $request->user_id;
            $attendance->date = Carbon::now()->toDateString();
            $attendance->latitude = $request->latitude;
            $attendance->longitude = $request->longitude;
            $attendance->save();

            return Response::json(['status' => 'success', 'data' => ['attendance' => $attendance]]);
        } else {
            return Response::json(['status' => 'success', 'data' => ['attendance' => $todaysAttendance]]);
        }
    }

    public function updateAttendance(Request $request, $id)
    {
        $attendance = Attendance::find($id);
        $attendance->status = $request->status;
        $attendance->save();

        return Response::json(['status' => 'success', 'data' => ['attendance' => $attendance]]);
    }

    public function getAttendanceStatus(Request $request, $id)
    {
        $attendance = Attendance::find($id);
        return Response::json(['status' => 'success', 'data' => ['attendance' => $attendance]]);
    }

    public function removeAttendance(){
        $attendance = Attendance::truncate();

        return Response::json(['status' => 'success', 'data' => ['attendance' => $attendance]]);
    }
}
