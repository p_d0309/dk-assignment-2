<?php

use App\Http\Controllers\AttendanceController;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::get('attendance/{id}', [AttendanceController::class, 'getAttendanceStatus']);
Route::post('attendance', [AttendanceController::class, 'storeAttendance']);
Route::post('attendance/remove', [AttendanceController::class, 'removeAttendance']);
Route::middleware('auth:sanctum')->get('/user', function (Request $request) {
    return $request->user();
});
