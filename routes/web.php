<?php

use App\Models\Attendance;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    $attendances = Attendance::all();

    return view('welcome', compact('attendances'));
});

Route::post('attendance/{id}', function ($id) {
    $attendance = Attendance::find($id);
    $attendance->status = 'received';
    $attendance->save();

    return redirect()->back();
})->name('mark-attendance');
